import { useEffect, useState } from 'react'
import moment from 'moment'
import { Tabs, Tab } from 'react-bootstrap'
import MonthlyChart from '../components/MonthlyChart'

export default function insights() {

	const [distances, setDistances] = useState([])
	const [durations, setDurations] = useState([])
	const [expenditures, setExpenditures] = useState([])

	useEffect(() => {
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`, {
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data)

			if(data.travels.length >0){
				let monthlyDistance = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
				let monthlyDuration = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
				let monthlyExpenditure = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

				data.travels.forEach(travel => {
					//January = 0, February = 1, ...
					const index = moment(travel.date).month()

					monthlyDistance[index] += (travel.distance/1000)
					//monthlyDistance[index] = monthlyDistance[index] + (travel.distance/1000)
					monthlyDuration[index] += (parseInt(travel.duration)/60)
					monthlyExpenditure[index] += (travel.charge.amount)
				})

				setDistances(monthlyDistance)
				console.log(monthlyDistance)
				setDurations(monthlyDuration)
				console.log(monthlyDuration)
				setExpenditures(monthlyExpenditure)
				console.log(monthlyExpenditure)
			}
		})
	}, [])

	return (
		// <h1>Hello World!</h1>
		<Tabs defaultActiveKey="distances" id="monthlyFigures">
			<Tab eventKey="distances" title="Monthly Distance Travelled">
				<MonthlyChart figuresArray={distances} label="Monthly total in kilometers"/>
			</Tab>
			<Tab eventKey="durations" title="Monthly Travel Durations">
				<MonthlyChart figuresArray={durations} label="Monthly Travel Durations"/>
			</Tab>
			<Tab eventKey="expenditures" title="Monthly Travel Expenditures">
				<MonthlyChart figuresArray={expenditures} label="Monthly Travel Expenditures"/>
			</Tab>
		</Tabs>
		// <Tabs>
			
		// </Tabs>
		)
}

// Visualize monthly travel durations and monthly travel expenditures.

			// const [durations, setDurations] = useState([])
			// const [expenditures, setExpenditures] = useState([])

			// let monthlyDuration = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
			// let monthlyExpenditure = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

			// setDurations(monthlyDuration)
			// console.log(monthlyDuration)
			// setExpenditures(monthlyExpenditure)
			// console.log(monthlyExpenditure)
			
			// <Tab eventKey="durations" title="Monthly Travel Durations">
			// 	<MonthlyChart figuresArray={durations} label="Monthly Travel Durations"/>
			// </Tab>
			// <Tab eventKey="expenditures" title="Monthly Travel Expenditures">
			// 	<MonthlyChart figuresArray={expenditures} label="Monthly Travel Expenditures"/>
			// </Tab>